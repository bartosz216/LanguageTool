﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tatarski.Apka.INTERFACES;
using Tatarski.PW_Projekt.UI.ViewModel.Command;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    class LearnViewModel : ChildViewModel
    {
        private string _translate;
        private IWord _word1;
        private IWord _word2;
        private IDictionary _dictionary1;
        private IDictionary _dictionary2;
        private ICategory _category;
        private CommonCommand _getTranslateCommand;
        private CommonCommand _checkCommand;
        private CommonCommand _pressEnterCommand;
        private bool _isCorrect;
        private bool _isChecked;

        public LearnViewModel(BaseViewModel parent, IDictionary dic1, IDictionary dic2, ICategory cat)
            : base(parent)
        {
            _dictionary1 = dic1;
            _dictionary2 = dic2;
            _category = cat;
        }

        public string Translate
        {
            get { return _translate; }
            set
            {
                _translate = value;
                OnPropertyChanged("Translate");
            }
        }

        public bool IsCorrect
        {
            get { return _isCorrect; }
            set 
            {
                _isCorrect = value;
                OnPropertyChanged("IsCorrect");
            }
        }

        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value;
                OnPropertyChanged("IsChecked");
            }
        }
        
        public string Word
        {
            get
            {
                if (_word1 == null)
                    return String.Empty;

                return _word1.Name; 
            }
        }

        public CommonCommand CheckCommand
        {
            get
            {
                if (_checkCommand == null)
                    _checkCommand = new CommonCommand(CheckAction,
                        param => _word1 != null && _word2 != null);

                return _checkCommand;
            }
        }

        public CommonCommand GetTranslateCommand
        {
            get
            {
                if (_getTranslateCommand == null)
                    _getTranslateCommand = new CommonCommand(GetTranslateAction,
                        param => IsCorrect || (_word1 == null && _word2 == null));
                return _getTranslateCommand;
            }
        }


        public CommonCommand PressEnterCommand
        {
            get
            {
                if (_pressEnterCommand == null)
                    _pressEnterCommand = new CommonCommand(PressEnterAction);
                return _pressEnterCommand;
            }
        }

        private void CheckAction(object obj)
        {
            IsCorrect = _word2.Name == Translate;
            IsChecked = true;
            AddKnownledge();
            if(IsCorrect)
                _word1 = _word2 = null;
        }

        private void GetTranslateAction(object obj)
        {
            DLLLoader.Instance.GetWord(_dictionary1, _dictionary2, _category, out _word1, out _word2);
            IsCorrect = IsChecked = false;
            Translate = "";
            OnPropertyChanged("Word");
        }


        private void PressEnterAction(object obj)
        {
            if (CheckCommand.CanExecute(null))
                CheckCommand.Execute(null);
        }

        private void AddKnownledge()
        {
            if (_word1 != null && _word2 != null && !string.IsNullOrWhiteSpace(Translate))
                DLLLoader.Instance.AddKnowledge(_word1, _word2, IsCorrect);
        }
        

    }
}
