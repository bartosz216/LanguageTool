﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.PW_Projekt.UI.ViewModel.Command;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    public abstract class ListViewModel<T>: ChildViewModel
    {

        #region Fields

        private ObservableCollection<T> _list;
        private ObservableCollection<T> _mainList;
        private CommonCommand _lostFocusCommand;
        private CommonCommand _removeCommand;
        private CommonCommand _textChangedCommand;

        #endregion

        #region Constructors

        public ListViewModel(BaseViewModel parent, List<T> list)
            : base(parent)
        {
            if (list != null)
                MainList = new ObservableCollection<T>(list);
            else
                MainList = new ObservableCollection<T>();

            List = MainList;
        }

        #endregion

        #region Properies

        public ObservableCollection<T> List
        {
            get { return _list; }
            protected set
            {
                if (_list != value)
                {
                    _list = value;
                    OnPropertyChanged("List");
                }
            }
        }

        public ObservableCollection<T> MainList
        {
            get { return _mainList; }
            protected set
            {
                if (_mainList != value)
                {
                    _mainList = value;
                    List = MainList;
                    OnPropertyChanged("MainList");
                }
            }
        }

        public String SearchText { get; set; }

        public T SelectedItem { get; set; }

        #endregion

        #region Commands

        public CommonCommand RemoveCommand
        {
            get 
            {
                if (_removeCommand == null)
                {
                    _removeCommand = new CommonCommand(RemoveAction, RemovePredocate);
                }

                return _removeCommand; 
            }
        }

        public CommonCommand LostFocusCommand
        {
            get 
            {
                if (_lostFocusCommand == null)
                {
                    _lostFocusCommand = new CommonCommand(LostFocusAction);
                }
                return _lostFocusCommand; 
            }
        }

        public CommonCommand TextChangedCommand
        {
            get 
            {
                if (_textChangedCommand == null)
                {
                    _textChangedCommand = new CommonCommand(TextChangedAction);
                }
                return _textChangedCommand;
            }
        }    

        #endregion

        #region Methods

        protected virtual void RemoveAction(object obj)
        {
            if (TextChangedCommand.CanExecute(null))
            {
                TextChangedCommand.Execute(null);
            }

            //OnPropertyChanged("MainList");
        }
        
        protected bool RemovePredocate(object obj)
        {
            return SelectedItem != null;
        }

        protected abstract void LostFocusAction(object obj);

        protected abstract void TextChangedAction(object obj);

        #endregion

    }
}
