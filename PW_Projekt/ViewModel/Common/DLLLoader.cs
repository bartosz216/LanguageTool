﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;

namespace Tatarski.PW_Projekt.UI.ViewModel.Common
{
    class DLLLoader : IDataLoad
    {
        private static DLLLoader _dllLoader;
        private IDataLoad _dataLoad;

        private DLLLoader()
        { }

        public static DLLLoader LoadDLLLoader(string filePath)
        {
            _dllLoader = new DLLLoader();
            _dllLoader.Load(filePath);

            return _dllLoader;
        }

        public void Load(string filePath)
        {
            Assembly assembly = Assembly.UnsafeLoadFrom(filePath);
            foreach (Type type in assembly.GetTypes())
            {
                if (type.GetInterfaces().Contains(typeof(IDataLoad)))
                {
                    _dataLoad = Activator.CreateInstance(type) as IDataLoad;
                }
            }
        }

        public static DLLLoader Instance
        {
            get
            {
                if (_dllLoader == null)
                {
                    _dllLoader = new DLLLoader();
                    _dllLoader.Load(Properties.Settings.Default.DAOFile);
                }
                return _dllLoader;
            }
        }

        public bool IsReadCorrect
        {
            get { return _dataLoad != null; }
        }

        public bool Login(string name, string password)
        {
            return _dataLoad.Login(name, password);
        }

        public bool Logout()
        {
            return _dataLoad.Logout();
        }

        public List<ICategory> Categories
        {
            get { return _dataLoad.Categories; }
        }

        public List<IDictionary> Dictionaries
        {
            get { return _dataLoad.Dictionaries; }
        }

        public List<IKnowledge> Statistics
        {
            get { return _dataLoad.Statistics; }
        }

        public List<ITranslate> Translates
        {
            get { return _dataLoad.Translates; }
        }

        public IUser User
        {
            get { return _dataLoad.User; }
        }

        public List<IWord> Words
        {
            get { return _dataLoad.Words; }
        }

        public List<IWord_Category> WordsCategories
        {
            get { return _dataLoad.WordsCategories; }
        }

        public bool IsLogin
        {
            get { return _dataLoad.IsLogin; }
        }

        public bool AddUser(string userName, string userPassword)
        {
            return _dataLoad.AddUser(userName, userPassword);
        }

        public List<ICategory> AddCategory(string categoryName)
        {
            return _dataLoad.AddCategory(categoryName);
        }

        public List<IDictionary> AddDictionary(string dictionaryName)
        {
            return _dataLoad.AddDictionary(dictionaryName);
        }

        public List<IKnowledge> AddKnowledge(IWord word1, IWord word2, bool isCorrect)
        {
            return _dataLoad.AddKnowledge(word1, word2, isCorrect);
        }

        public List<ITranslate> AddTranslate(IWord word1, IWord word2)
        {
            return _dataLoad.AddTranslate(word1, word2);
        }

        public List<IWord> AddWord(string wordName, IDictionary dictionary)
        {
            return _dataLoad.AddWord(wordName, dictionary);
        }

        public List<IWord_Category> AddWordCategory(IWord word, ICategory category)
        {
            return _dataLoad.AddWordCategory(word, category);
        }

        public List<ICategory> RemoveCategory(ICategory category)
        {
            return _dataLoad.RemoveCategory(category);
        }

        public List<IDictionary> RemoveDictionary(IDictionary dictionaty)
        {
            return _dataLoad.RemoveDictionary(dictionaty);
        }

        public bool RemoveStatistics()
        {
            return _dataLoad.RemoveStatistics();
        }

        public List<ITranslate> RemoveTranslate(ITranslate translate)
        {
            return _dataLoad.RemoveTranslate(translate);
        }

        public bool RemoveUser()
        {
            return _dataLoad.RemoveUser();
        }

        public List<IWord> RemoveWord(IWord word)
        {
            return _dataLoad.RemoveWord(word);
        }

        public List<IWord_Category> RemoveWordCategory(IWord word, ICategory category)
        {
            return _dataLoad.RemoveWordCategory(word, category);
        }

        public List<ICategory> UpdateCategory(ICategory category)
        {
            return _dataLoad.UpdateCategory(category);
        }

        public List<IDictionary> UpdateDictionaty(IDictionary dictionary)
        {
            return _dataLoad.UpdateDictionaty(dictionary);
        }

        public List<IKnowledge> UpdateStatistics(IKnowledge knowledge)
        {
            return _dataLoad.UpdateStatistics(knowledge);
        }

        public List<ITranslate> UpdateTranslate(ITranslate translate)
        {
            return _dataLoad.UpdateTranslate(translate);
        }

        public IUser UpdateUser(string newPassword)
        {
            return _dataLoad.UpdateUser(newPassword);
        }

        public List<IWord> UpdateWord(IWord word)
        {
            return _dataLoad.UpdateWord(word);
        }

        public void GetWord(IDictionary dic1, IDictionary dic2, ICategory cat, out IWord word1, out IWord word2)
        {
            _dataLoad.GetWord(dic1, dic2, cat, out word1, out word2);
        }

    }
}
