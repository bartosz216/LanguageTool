﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Tatarski.PW_Projekt.UI.ViewModel.Command;

namespace Tatarski.PW_Projekt.UI.ViewModel.Common
{
    public abstract class BaseViewModel : INotifyPropertyChanged, INotifyDataErrorInfo
    {

        #region Fields

        private string _name;
        protected readonly Dictionary<string, ICollection<string>> _validationErroes = new Dictionary<string, ICollection<string>>();

        #endregion

        #region Properties

        public abstract List<BaseViewModel> Views { get; }

        public abstract BaseViewModel CurrentView { get; set; }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }

        }

        #endregion

        #region Command

        public abstract CommonCommand ChangeViewCommand { get; }

        #endregion

        #region Methods

        public void ChangeView(BaseViewModel baseViewModel)
        {
            if (!Views.Contains(baseViewModel))
            {
                Views.Add(baseViewModel);
            }

            CurrentView = Views.FirstOrDefault(vm => vm == baseViewModel);
        }

        protected void RaiseErrorChanged(string propertyName)
        {
            if (ErrorsChanged != null)
            {
                ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region CloseCommand

        CommonCommand _closeCommand;

        public ICommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new CommonCommand(param => this.OnRequestClose());
                }
                return _closeCommand;
            }
        }


        public event EventHandler RequestClose;

        protected void OnRequestClose()
        {
            EventHandler handler = this.RequestClose;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string a_propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(a_propertyName));
            }
        }

        #endregion

        #region INotifyDataErrorInfo Members

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName) || !_validationErroes.ContainsKey(propertyName))
            {
                return null;
            }

            return _validationErroes[propertyName];
        }

        public bool HasErrors
        {
            get { return _validationErroes.Count > 0; }
        }

        #endregion

    }
}
