﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;

namespace Tatarski.PW_Projekt.UI.ViewModel.Common
{
    class LoginEventArgs : EventArgs 
    {
        public LoginEventArgs(IUser user)
        {
            User = user; 
        }

        public IUser User { get; private set; }
    }
}
