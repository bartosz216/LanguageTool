﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.PW_Projekt.UI.ViewModel.Command;

namespace Tatarski.PW_Projekt.UI.ViewModel.Common
{
    public abstract class ChildViewModel : BaseViewModel
    {
        #region Fields

        protected BaseViewModel _parent;

        #endregion

        #region Constructors

        public ChildViewModel(BaseViewModel parent)
        {
            if (parent == null)
                throw new ArgumentNullException(parent.ToString());

            _parent = parent;

            EventHandler handler = null;

            handler = delegate
            {
                ChangeView(_parent);
            };

            RequestClose += handler;
        }

        #endregion

        #region Properties

        public override List<BaseViewModel> Views
        {
            get
            {
                return _parent.Views;
            }
        }

        public override BaseViewModel CurrentView
        {
            get
            {
                return _parent.CurrentView;
            }
            set
            {
                if (_parent.CurrentView != value)
                {
                    _parent.CurrentView = value;
                    OnPropertyChanged("CurrentView");
                }
            }
        }

        #endregion

        #region Command

        public override CommonCommand ChangeViewCommand
        {
            get
            {
                return _parent.ChangeViewCommand;
            }
        }



        #endregion
    }
}
