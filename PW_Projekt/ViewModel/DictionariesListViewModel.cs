﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    class DictionariesListViewModel : AddingListViewModel<IDictionary>
    {
        public DictionariesListViewModel(BaseViewModel parent, List<IDictionary> list)
            : base(parent, list)
        { }

        protected override void AddAction(object obj)
        {
            MainList = new ObservableCollection<IDictionary>(DLLLoader.Instance.AddDictionary(AddText));
            base.AddAction(obj);
        }

        protected override void RemoveAction(object obj)
        {
            if (SelectedItem != null)
            {
                MainList = new ObservableCollection<IDictionary>(DLLLoader.Instance.RemoveDictionary(SelectedItem));
                base.RemoveAction(obj);
            }
        }

        protected override void LostFocusAction(object obj)
        {
            MainList = new ObservableCollection<IDictionary>(DLLLoader.Instance.UpdateDictionaty(SelectedItem));
        }

        protected override void TextChangedAction(object obj)
        {
            if (SearchText != null)
            {
                List = new ObservableCollection<IDictionary>(from l in MainList where l.Name.StartsWith(SearchText) select l);
            }
        }
    }
}
