﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;
using Tatarski.PW_Projekt.UI.ViewModel.Command;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    class WordsListViewModel : ChildViewModel
    {
        private CommonCommand _textChangedCommand;
        private ObservableCollection<IWord> _displayWords;
        private ObservableCollection<IWord> _allWords;
        private ICategory _category;
        private IDictionary _dictionary;
        private IWord _selectedWord;


        public WordsListViewModel(BaseViewModel parent)
            : base(parent)
        { }

        public ObservableCollection<IWord> AllWords
        {
            get
            {
                if (_allWords == null)
                    _allWords = new ObservableCollection<IWord>(DLLLoader.Instance.Words);

                return _allWords;
            }
            set
            {
                _allWords = value;
                OnPropertyChanged("AllWords");
            }
        }

        public ObservableCollection<IWord> DisplayWords
        {
            get
            {
                if (_displayWords == null )
                {
                    _displayWords = new ObservableCollection<IWord>(DLLLoader.Instance.Words);
                }

                var list = new ObservableCollection<IWord>(DLLLoader.Instance.Words);
                if (AllWords.Count != list.Count)
                {
                    AllWords = list;

                    if (TextChangedCommand.CanExecute(null))
                        TextChangedCommand.Execute(null);
                }

                return _displayWords; 
            }
            set
            {
                _displayWords = value;
                OnPropertyChanged("DisplayWords");
            }
        }

        public ObservableCollection<IDictionary> Dictionaries
        {
            get { return new ObservableCollection<IDictionary>(DLLLoader.Instance.Dictionaries); }
        }

        public ObservableCollection<ICategory> Categories
        {
            get { return new ObservableCollection<ICategory>(DLLLoader.Instance.Categories); ; }
        }

        public IWord SelectedWord
        {
            get { return _selectedWord; }
            set
            {
                if (_selectedWord != value)
                {
                    _selectedWord = value;
                    OnPropertyChanged("SelectedWord");
                }
            }
        }

        public string SearchText { get; set; }

        public CommonCommand TextChangedCommand
        {
            get
            {
                if (_textChangedCommand == null)
                {
                    _textChangedCommand = new CommonCommand(TextChangedAction);
                }
                return _textChangedCommand;
            }
        }

        public ICategory Category
        {
            get { return _category; }
            set
            {
                _category = value;

                if (TextChangedCommand.CanExecute(null))
                    TextChangedCommand.Execute(null);

                OnPropertyChanged("Category");
            }
        }

        public IDictionary Dictionary
        {
            get { return _dictionary; }
            set
            {
                _dictionary = value;

                if (TextChangedCommand.CanExecute(null))
                    TextChangedCommand.Execute(null);

                OnPropertyChanged("Dictionary");
            }
        }


        private void TextChangedAction(object obj)
        {
            string searchText = SearchText == null ? "" : SearchText;
            if (Dictionary == null && Category == null)
            {
                DisplayWords = new ObservableCollection<IWord>(
                    from word in DLLLoader.Instance.Words
                    where word.Name.StartsWith(searchText)
                    select word
                    );
            }
            else if (Dictionary == null)
            {
                DisplayWords = new ObservableCollection<IWord>(
                    from word in DLLLoader.Instance.Words
                    from wc in DLLLoader.Instance.WordsCategories
                    where word.Name.StartsWith(searchText) &&
                    wc.WordID == word.ID && wc.CategoryID == Category.ID
                    select word
                    );
            }
            else if (Category == null)
            {
                DisplayWords = new ObservableCollection<IWord>(
                    from word in DLLLoader.Instance.Words
                    where word.Name.StartsWith(searchText) && word.DictionaryID == Dictionary.ID
                    select word
                    );
            }
            else
            {
                DisplayWords = new ObservableCollection<IWord>(
                    from word in DLLLoader.Instance.Words
                    from wc in DLLLoader.Instance.WordsCategories
                    where word.Name.StartsWith(searchText) && word.DictionaryID == Dictionary.ID
                    && wc.WordID == word.ID && wc.CategoryID == Category.ID
                    select word
                    );
            }
        }

    }
}
