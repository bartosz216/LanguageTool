﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;
using Tatarski.PW_Projekt.UI.ViewModel.Command;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    class EditWordViewModel : ChildViewModel
    {

        #region Fields

        private CommonCommand _removeWordCommand;
        private CommonCommand _addWordCommand;
        private CommonCommand _changeDictionaryCommand;
        private CommonCommand _addCategoryCommand;
        private CommonCommand _removeCategoryCommand;
        private CommonCommand _textChangedCommand;
        private CommonCommand _lostFocusCommand;
        private ObservableCollection<IWord> _allWords;
        private ObservableCollection<IWord> _dispayWords;
        private ICategory _category;
        private IDictionary _dictionary;
        private IWord _selectedWord;

        #endregion

        #region Constructors

        public EditWordViewModel(BaseViewModel parent)
            : base(parent)
        {
            if (DLLLoader.Instance.Words != null)
                AllWords = new ObservableCollection<IWord>(DLLLoader.Instance.Words);
            else
                AllWords = new ObservableCollection<IWord>();

            DisplayWords = AllWords;
        }

        #endregion

        #region Properties

        public ObservableCollection<IWord> AllWords
        {
            get { return _allWords; }
            set
            {
                if (_allWords != value)
                {
                    _allWords = value;

                    if (TextChangedCommand.CanExecute(null))
                        TextChangedCommand.Execute(null);

                    OnPropertyChanged("AllWords");
                }
            }
        }


        public ObservableCollection<IWord> DisplayWords
        {
            get 
            {
                if (_allWords.Count != DLLLoader.Instance.Words.Count)
                {
                    _allWords = new ObservableCollection<IWord>(DLLLoader.Instance.Words);
                    OnPropertyChanged("AllWords");
                    _dispayWords = _allWords;
                    OnPropertyChanged("DisplayWords");
                }
                return _dispayWords; 
            }
            set
            {
                _dispayWords = value;
                OnPropertyChanged("DisplayWords");
            }
        }

        public ObservableCollection<IDictionary> Dictionaries
        {
            get
            {
                var list = new ObservableCollection<IDictionary>(DLLLoader.Instance.Dictionaries);
                return list;
            }
        }

        public ObservableCollection<ICategory> Categories
        {
            get { return new ObservableCollection<ICategory>(DLLLoader.Instance.Categories); ; }
        }

        public ObservableCollection<ICategory> WordCategories
        {
            get
            {
                if (SelectedWord != null && Categories != null)
                {
                    return new ObservableCollection<ICategory>(
                        from c in Categories
                        join wc in DLLLoader.Instance.WordsCategories
                        on c.ID equals wc.CategoryID
                        where SelectedWord.ID == wc.WordID
                        select c
                        );
                }
                return null;
            }
        }

        public IWord SelectedWord
        {
            get { return _selectedWord; }
            set
            {
                if (_selectedWord != value)
                {
                    _selectedWord = value;
                    OnPropertyChanged("SelectedWord");
                    OnPropertyChanged("CanChange");
                    OnPropertyChanged("DictionaryName");
                    OnPropertyChanged("WordCategories");
                }
            }
        }

        public string SearchText { get; set; }

        public string WordName { get; set; }

        public string DictionaryName
        {
            get
            {
                if(SelectedWord != null)
                    return (from d in Dictionaries where d.ID == SelectedWord.DictionaryID select d).First().Name;

                return "";
            }
        }

        public bool CanChange
        {
            get
            {
                return SelectedWord != null;
            }
        }

        public IDictionary SelectedChangeDictionary { get; set; }

        public ICategory SelectedAddCategory { get; set; }

        public ICategory SelectedRemoveCategory { get; set; }

        public IDictionary SelectedAddDictionary { get; set; }

        public CommonCommand AddWordCommand
        {
            get
            {
                if (_addWordCommand == null)
                {
                    _addWordCommand = new CommonCommand(AddWordAction,
                        param => !string.IsNullOrEmpty(WordName) && SelectedAddDictionary != null);
                }
                return _addWordCommand;
            }
        }

        public CommonCommand RemoveWordCommand
        {
            get
            {
                if (_removeWordCommand == null)
                {
                    _removeWordCommand = new CommonCommand(RemoveWordAction,
                        param => SelectedWord != null);
                }
                return _removeWordCommand;
            }
        }

        public CommonCommand RemoveCategoryCommand
        {
            get
            {
                if (_removeCategoryCommand == null)
                {
                    _removeCategoryCommand = new CommonCommand(RemoveCategoryAction,
                        param => SelectedRemoveCategory != null);
                }
                return _removeCategoryCommand;
            }
        }

        public CommonCommand AddCategoryCommand
        {
            get
            {
                if (_addCategoryCommand == null)
                {
                    _addCategoryCommand = new CommonCommand(AddCategoryAction,
                        param => SelectedAddCategory != null);
                }
                return _addCategoryCommand;
            }
        }

        public CommonCommand ChangeDictionaryCommand
        {
            get
            {
                if (_changeDictionaryCommand == null)
                {
                    _changeDictionaryCommand = new CommonCommand(ChangeDictionaryAction,
                        param => SelectedChangeDictionary != null);
                }
                return _changeDictionaryCommand;
            }
        }

        public CommonCommand TextChangedCommand
        {
            get
            {
                if (_textChangedCommand == null)
                {
                    _textChangedCommand = new CommonCommand(TextChangedAction);
                }
                return _textChangedCommand;
            }
        }

        public CommonCommand LostFocusCommand
        {
            get
            {
                if (_lostFocusCommand == null)
                    _lostFocusCommand = new CommonCommand(LostFocusAction);
                return _lostFocusCommand; 
            }

        }     

        public ICategory Category
        {
            get { return _category; }
            set
            {
                _category = value;
                OnPropertyChanged("Category");
                TextChangedAction(null);
            }
        }

        public IDictionary Dictionary
        {
            get { return _dictionary; }
            set
            {
                _dictionary = value;
                OnPropertyChanged("Dictionary");
                TextChangedAction(null);
            }
        }

        #endregion

        #region Methods

        private void AddWordAction(object obj)
        {
            AllWords = new ObservableCollection<IWord>(DLLLoader.Instance.AddWord(WordName, SelectedAddDictionary));
            WordName = "";
            SelectedAddDictionary = null;
            OnPropertyChanged("WordName");
            OnPropertyChanged("SelectedAddDictionary");
        }


        private void RemoveWordAction(object obj)
        {
            AllWords = new ObservableCollection<IWord>(DLLLoader.Instance.RemoveWord(SelectedWord));
        }

        private void RemoveCategoryAction(object obj)
        {
            DLLLoader.Instance.RemoveWordCategory(SelectedWord, SelectedRemoveCategory);
            OnPropertyChanged("WordCategories");
            SelectedRemoveCategory = null;
            OnPropertyChanged("SelectedRemoveCategory");
        }

        private void AddCategoryAction(object obj)
        {
            DLLLoader.Instance.AddWordCategory(SelectedWord, SelectedAddCategory);
            OnPropertyChanged("WordCategories");
            SelectedAddCategory = null;
            OnPropertyChanged("SelectedAddCategory");
        }

        private void ChangeDictionaryAction(object obj)
        {
            SelectedWord.DictionaryID = SelectedChangeDictionary.ID;
            DLLLoader.Instance.UpdateWord(SelectedWord);
            OnPropertyChanged("DictionaryName");
            SelectedChangeDictionary = null;
            OnPropertyChanged("SelectedChangeDictionary");
        }


        private void TextChangedAction(object obj)
        {
            string searchText = SearchText == null ? "" : SearchText;
            if (Dictionary == null && Category == null)
            {
                DisplayWords = new ObservableCollection<IWord>(
                    from word in AllWords
                    where word.Name.StartsWith(searchText)
                    select word
                    );
            }
            else if (Dictionary == null)
            {
                DisplayWords = new ObservableCollection<IWord>(
                    from word in AllWords
                    from wc in DLLLoader.Instance.WordsCategories
                    where word.Name.StartsWith(searchText) &&
                    wc.WordID == word.ID && wc.CategoryID == Category.ID
                    select word
                    );
            }
            else if (Category == null)
            {
                DisplayWords = new ObservableCollection<IWord>(
                    from word in AllWords
                    where word.Name.StartsWith(searchText) && word.DictionaryID == Dictionary.ID
                    select word
                    );
            }
            else
            {
                DisplayWords = new ObservableCollection<IWord>(
                    from word in AllWords
                    from wc in DLLLoader.Instance.WordsCategories
                    where word.Name.StartsWith(searchText) && word.DictionaryID == Dictionary.ID
                    && wc.WordID == word.ID && wc.CategoryID == Category.ID
                    select word
                    );
            }
        }


        private void LostFocusAction(object obj)
        {
            AllWords = new ObservableCollection<IWord>(DLLLoader.Instance.UpdateWord(SelectedWord));
        }

        #endregion

    }
}
