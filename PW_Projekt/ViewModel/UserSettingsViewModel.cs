﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.PW_Projekt.UI.ViewModel.Command;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    public class UserSettingsViewModel : ChildViewModel
    {
        private CommonCommand _removeUserCommand;
        private CommonCommand _chechDAOFileCommand;
        private string _daoPath;


        public UserSettingsViewModel(BaseViewModel parent)
            : base(parent)
        {
            DAOPath = Properties.Settings.Default.DAOFile;
        }

        public CommonCommand RemoveUserCommand
        {
            get
            {
                if (_removeUserCommand == null)
                    _removeUserCommand = new CommonCommand(RemoveUserAction,
                        param => IsLogin);
                return _removeUserCommand;
            }
        }

        public bool IsLogin 
        { 
            get { return DLLLoader.Instance.IsLogin; } 
        }

        public string DAOPath
        {
            get { return _daoPath; }
            set 
            {
                _daoPath = value;
                OnPropertyChanged("DAOPath");
            }
        }


        public CommonCommand ChechDAOFileCommand
        {
            get
            {
                if (_chechDAOFileCommand == null)
                    _chechDAOFileCommand = new CommonCommand(ChechDAOFileAction,
                        param => Properties.Settings.Default.DAOFile != DAOPath);
                return _chechDAOFileCommand;
            }
        }


        private void ChechDAOFileAction(object obj)
        {
            if (ValidateDAOPath())
            {
                Properties.Settings.Default.DAOFile = DAOPath;
                Properties.Settings.Default.Save();
            }
        }

        private void RemoveUserAction(object obj)
        {
            DLLLoader.Instance.RemoveUser();
        }



        private bool ValidateDAOPath()
        {
            List<string> errors = new List<string>();

            if (!string.IsNullOrWhiteSpace(DAOPath))
            {
                try
                {
                    DLLLoader.LoadDLLLoader(DAOPath);
                }
                catch(Exception)
                {
                    errors.Add("Podałeś nieprawidłową ściężkę do pliku .dll");
                }
            }
            else
                errors.Add("Podaj ścieżkę do pliku .dll");

            if (_validationErroes.ContainsKey("DAOPath"))
                _validationErroes.Remove("DAOPath");

            if (errors.Count > 0)
            {
                _validationErroes["DAOPath"] = errors;
                RaiseErrorChanged("DAOPath");
                return false;
            }

            return true;
        }
    }
}
