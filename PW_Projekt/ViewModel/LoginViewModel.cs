﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security;
using Tatarski.PW_Projekt.UI.ViewModel.Command;
using Tatarski.PW_Projekt.UI.ViewModel.Common;
using Tatarski.Apka.INTERFACES;
using System.Windows;
using System.Diagnostics;
using Tatarski.PW_Projekt.UI.View;
using System.ComponentModel;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    class LoginViewModel : ChildViewModel
    {
        public String UserName { get; set; }
        public String Password { get; set; }
        public bool IsLogin { get; set; }

        public CommonCommand LoginCommand { get; set; }
        public CommonCommand AddUserCommand { get; set; }

        public LoginViewModel(BaseViewModel parent)
            : base(parent)
        {
            LoginCommand = new CommonCommand(LoginAction, LoginPredicate);
            AddUserCommand = new CommonCommand(AddUserAction);
        }

        private bool LoginPredicate(object obj)
        {
            return !String.IsNullOrWhiteSpace(UserName);
        }

        private void LoginAction(object obj)
        {
            if ((IsLogin = DLLLoader.Instance.Login(UserName, Password)) == false)
            {

                List<string> error = new List<string>();
                error.Add("Podałeś złe hasło lub nazwę użytkownika");

                if (_validationErroes.ContainsKey("UserName"))
                    _validationErroes.Remove("UserName");

                if (_validationErroes.ContainsKey("Password"))
                    _validationErroes.Remove("Password");

                _validationErroes["UserName"] = error;
                _validationErroes["Password"] = error;

                RaiseErrorChanged("UserName");
                RaiseErrorChanged("Password");

            }
            else
            {
                ChangeView(_parent);
            }

            OnPropertyChanged("IsLogin");
            
        }

        private void AddUserAction(object obj)
        {
            ChangeView(new AddUserViewModel(this));
        }
        
    }
}
