﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;
using Tatarski.PW_Projekt.UI.ViewModel.Command;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    public class MainWindowViewModel : BaseViewModel
    {
        #region Fields

        private CommonCommand _changeViewCommand;
        private BaseViewModel _currentView;
        private List<BaseViewModel> _views;

        #endregion

        #region Constructors

        public MainWindowViewModel()
        { }

        #endregion

        #region Properties

        public override List<BaseViewModel> Views
        {
            get
            {
                if (_views == null)
                {
                    _views = new List<BaseViewModel>();
                }

                return _views;
            }
        }

        public override BaseViewModel CurrentView
        {
            get
            {
                return _currentView;
            }
            set
            {
                if (_currentView != value)
                {
                    _currentView = value;
                    OnPropertyChanged("CurrentView");
                }
            }
        }

        #endregion

        #region Command

        public override CommonCommand ChangeViewCommand
        {
            get
            {
                if (_changeViewCommand == null)
                {
                    _changeViewCommand = new CommonCommand(
                        p => ChangeView((BaseViewModel)p),
                        p => p is BaseViewModel);
                }
                return _changeViewCommand;
            }
        }

        #endregion
    }
}
