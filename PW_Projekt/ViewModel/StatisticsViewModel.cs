﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    public class StatisticsViewModel : ChildViewModel
    {

        public ObservableCollection<IKnowledge> Statistics
        {
            get { return new ObservableCollection<IKnowledge>(DLLLoader.Instance.Statistics); }
        }
        

        public StatisticsViewModel(BaseViewModel parent)
            : base(parent)
        { }
    }
}
