﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Tatarski.PW_Projekt.UI.ViewModel.Converter
{
    public class ComboBoxEmptyItemConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            IEnumerable container = value as IEnumerable;
            if (container != null)
            {
                // everything inherits from object, so we can safely create a generic IEnumerable
                IEnumerable<object> genericContainer = container.OfType<object>();
                // create an array with a single EmptyItem object that serves to show en empty line
                IEnumerable<object> emptyItem = new object[] { string.Empty };
                // use Linq to concatenate the two enumerable
                return emptyItem.Concat(genericContainer); 
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
