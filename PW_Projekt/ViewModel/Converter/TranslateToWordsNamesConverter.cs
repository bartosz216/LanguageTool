﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Tatarski.Apka.INTERFACES;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel.Converter
{
    public class TranslateToWordsNamesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int translateID = (int)value;
            ITranslate translate = (from t in DLLLoader.Instance.Translates where t.ID == translateID select t).First();
            IWord word1 = (from word in DLLLoader.Instance.Words where word.ID == translate.Word1ID select word).First();
            IWord word2 = (from word in DLLLoader.Instance.Words where word.ID == translate.Word2ID select word).First();
            return word1.Name + " <-> " + word2.Name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
