﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tatarski.Apka.INTERFACES;
using Tatarski.PW_Projekt.UI.Properties;
using Tatarski.PW_Projekt.UI.View;
using Tatarski.PW_Projekt.UI.ViewModel.Command;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    class MainViewModel : ChildViewModel
    {
        private CommonCommand _loginCommand;
        private CommonCommand _editDataCommand;
        private CommonCommand _watchStatisticsCommand;
        private CommonCommand _startLearningCommand;

        public MainViewModel(BaseViewModel parent)
            : base(parent)
        {
        }

        public bool IsLogin
        {
            get { return DLLLoader.Instance.IsLogin; }
        }

        public string LoginButtonText
        {
            get
            {
                if (!IsLogin)
                {
                    return Resources.Login;
                }
                return Resources.Logout;
            }
        }

        public string LoginButtonTip
        {
            get
            {
                if (!IsLogin)
                {
                    return Resources.LoginTip;
                }
                return Resources.LogoutTip;
            }
        }

        public ObservableCollection<IDictionary> Dictionaries
        {
            get
            {
                return new ObservableCollection<IDictionary>(DLLLoader.Instance.Dictionaries);
            }
        }

        public ObservableCollection<ICategory> Categories
        {
            get
            {
                return new ObservableCollection<ICategory>(DLLLoader.Instance.Categories);
            }
        }

        public IDictionary SelectedDictionart1 { get; set; }

        public IDictionary SelectedDictionary2 { get; set; }

        public ICategory SelectedCategory { get; set; }


        #region Commands

        public CommonCommand LoginCommand
        {
            get
            {
                if(_loginCommand == null)
                    _loginCommand = new CommonCommand(LoginAction);

                return _loginCommand;
            }
        }

        public CommonCommand EditDataCommand
        {
            get
            {
                if(_editDataCommand == null)
                    _editDataCommand = new CommonCommand(EditDataAction);

                return _editDataCommand;
            }
        }
        public CommonCommand WatchStatisticsCommand
        {
            get
            {
                if (_watchStatisticsCommand == null)
                    _watchStatisticsCommand = new CommonCommand(WatchStatisticsAction,
                        param => IsLogin);

                return _watchStatisticsCommand;
            }
        }

        public CommonCommand StartLearningCommand
        {
            get
            {
                if (_startLearningCommand == null)
                    _startLearningCommand = new CommonCommand(StartLearningAction,
                            param => SelectedCategory != null && SelectedDictionart1 != null &&
                            SelectedDictionary2 != null && SelectedDictionart1 != SelectedDictionary2 && IsLogin);

                return _startLearningCommand;
            }
        }

        private void EditDataAction(object arg)
        {
            ChangeView(new EditDataViewModel(this));
        }

        private void LoginAction(object arg)
        {   
            if (IsLogin)
                DLLLoader.Instance.Logout();

            ChangeView(new LoginViewModel(this));
        }

        private void WatchStatisticsAction(object obj)
        {
            ChangeView(new StatisticsViewModel(this));
        }

        private void StartLearningAction(object obj)
        {
            ChangeView(new LearnViewModel(this, SelectedDictionart1, SelectedDictionary2, SelectedCategory));
        }

        #endregion

    }
}
