﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;
using Tatarski.PW_Projekt.UI.ViewModel.Command;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    class AddUserViewModel : ChildViewModel
    {
        public String UserName { get; set; }
        public String Password { get; set; }

        public CommonCommand AddUserCommand { get; set; }

        public AddUserViewModel(BaseViewModel parent)
            : base(parent)
        {
            AddUserCommand = new CommonCommand(AddUserAction, AddUserPredicate);
        }

        private bool AddUserPredicate(object obj)
        {
            return !String.IsNullOrWhiteSpace(UserName);
        }

        private void AddUserAction(object obj)
        {
            try
            {
                DLLLoader.Instance.AddUser(UserName, Password);
                ChangeView(_parent);
            }
            catch (Exception e)
            {
                List<string> error = new List<string>();
                error.Add(e.Message);

                if (_validationErroes.ContainsKey("UserName"))
                    _validationErroes.Remove("UserName");

                _validationErroes["UserName"] = error;
                RaiseErrorChanged("UserName");
            }
        }
    }
}
