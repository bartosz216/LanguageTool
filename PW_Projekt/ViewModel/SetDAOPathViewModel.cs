﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.PW_Projekt.UI.ViewModel.Command;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    public class SetDAOPathViewModel : ChildViewModel
    {

        private string _daoPath;
        private CommonCommand _setDAOPathCommand;
        private CommonCommand _loadedCommand;


        public SetDAOPathViewModel(BaseViewModel parent)
            : base(parent)
        { }


        public string DAOPath
        {
            get { return _daoPath; }
            set
            {
                _daoPath = value;
                OnPropertyChanged("DAOPath");
            }
        }

        public CommonCommand SetDAOPathCommand
        {
            get
            {
                if (_setDAOPathCommand == null)
                    _setDAOPathCommand = new CommonCommand(SetDAOPathAction,
                        param => !string.IsNullOrWhiteSpace(DAOPath));

                return _setDAOPathCommand;
            }
        }

        public CommonCommand LoadedCommand
        {
            get
            {
                if (_loadedCommand == null)
                    _loadedCommand = new CommonCommand(LoadedAction);
                return _loadedCommand;
            }
        }


        private void LoadedAction(object obj)
        {
            try
            {
                DLLLoader.LoadDLLLoader(Properties.Settings.Default.DAOFile);
                ChangeView(_parent);
            }
            catch (Exception) { }
        }


        private void SetDAOPathAction(object obj)
        {
            try
            {
                DLLLoader.LoadDLLLoader(DAOPath);
                Properties.Settings.Default.DAOFile = DAOPath;
                Properties.Settings.Default.Save();
                ChangeView(_parent);
            }
            catch (Exception)
            {
                List<string> errors = new List<string>();
                errors.Add("Podałeś nieprawidłową ściężkę do pliku .dll");

                if (_validationErroes.ContainsKey("DAOPath"))
                    _validationErroes.Remove("DAOPath");

                _validationErroes["DAOPath"] = errors;

                RaiseErrorChanged("DAOPath");
            }
        }
    }
}
