﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;
using Tatarski.PW_Projekt.UI.ViewModel.Command;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    class EditTranslateViewModel : ChildViewModel
    {
        private WordsListViewModel _checkWord1;
        private WordsListViewModel _checkWord2;
        private ObservableCollection<ITranslate> _translates;
        private CommonCommand _removeCommand;
        private CommonCommand _addCommand;

        public EditTranslateViewModel(BaseViewModel parent)
            : base(parent)
        {

        }


        public ITranslate SelectedTranslate { get; set; }

        public ObservableCollection<ITranslate> Translates
        {
            get
            {
                if (_translates == null || _translates.Count != DLLLoader.Instance.Translates.Count)
                    _translates = new ObservableCollection<ITranslate>(DLLLoader.Instance.Translates);

                return _translates;
            }
            set
            {
                if (_translates != value)
                {
                    _translates = value;
                    OnPropertyChanged("Translates");
                }
            }
        }

        public WordsListViewModel CheckWord2
        {
            get
            {
                if (_checkWord2 == null)
                    _checkWord2 = new WordsListViewModel(this) { Name = "Wybierz drugie słowo" };

                return _checkWord2;
            }
        }


        public WordsListViewModel CheckWord1
        {
            get
            {
                if (_checkWord1 == null)
                    _checkWord1 = new WordsListViewModel(this) { Name = "Wybierz pierwsze słowo" };
                return _checkWord1;
            }
        }

        public CommonCommand AddCommand
        {
            get
            {
                if (_addCommand == null)
                    _addCommand = new CommonCommand(AddAction,
                        param => CheckWord1.SelectedWord != null 
                            && CheckWord2.SelectedWord != null
                            && CheckWord1.SelectedWord.DictionaryID != CheckWord2.SelectedWord.DictionaryID);
                return _addCommand; 
            }
        }


        public CommonCommand RemoveCommand
        {
            get
            {
                if (_removeCommand == null)
                    _removeCommand = new CommonCommand(RemoveAction,
                        param => SelectedTranslate != null);
                return _removeCommand; }
        }
        

        private void AddAction(object obj)
        {
            Translates = new ObservableCollection<ITranslate>(
                DLLLoader.Instance.AddTranslate(CheckWord1.SelectedWord, CheckWord2.SelectedWord)
              );
        }

        private void RemoveAction(object obj)
        {
            Translates = new ObservableCollection<ITranslate>(DLLLoader.Instance.RemoveTranslate(SelectedTranslate));
        }


    }
}
