﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;
using Tatarski.PW_Projekt.UI.ViewModel.Command;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    class EditDataViewModel : ChildViewModel
    {

        #region Fields

        private BaseViewModel _currentEditView;
        private List<BaseViewModel> _editView;

        #endregion

        #region Constructors

        public EditDataViewModel(BaseViewModel parent)
            : base(parent)
        {
            EditViews.Add(new CategoriesListViewModel(this, DLLLoader.Instance.Categories) { Name = "Edytuj kategorie" });
            EditViews.Add(new DictionariesListViewModel(this, DLLLoader.Instance.Dictionaries) { Name = "Edytuj słowniki" });
            EditViews.Add(new EditWordViewModel(this) { Name = "Edytuj wyrazy" });
            EditViews.Add(new EditTranslateViewModel(this) { Name = "Edytuj tłumaczenia" });
            EditViews.Add(new UserSettingsViewModel(this) { Name = "Ustawienia użytkownika" });
            CurrentEditView = EditViews[0];
        }

        #endregion

        #region Propeties

        public BaseViewModel CurrentEditView
        {
            get
            {
                return _currentEditView;
            }
            set
            {
                if (_currentEditView != value)
                {
                    _currentEditView = value;
                    OnPropertyChanged("CurrentEditView");
                }
            }
        }

        public List<BaseViewModel> EditViews
        {
            get
            {
                if (_editView == null)
                    _editView = new List<BaseViewModel>();

                return _editView;
            }
        }

        #endregion

        #region ChangeEditViewCommand

        private CommonCommand _changeEditViewCommand;

        public CommonCommand ChangeEditViewCommand
        {
            get
            {
                if (_changeEditViewCommand == null)
                {
                    _changeEditViewCommand = new CommonCommand(
                        p => ChangeEditView((BaseViewModel)p),
                        p => p is BaseViewModel);
                }

                return _changeEditViewCommand;
            }
        }

        private void ChangeEditView(BaseViewModel editDataViewModel)
        {
            if (!EditViews.Contains(editDataViewModel))
                EditViews.Add(editDataViewModel);

            CurrentEditView = EditViews.FirstOrDefault(e => e == editDataViewModel);
        }

        #endregion
       

    }
}
