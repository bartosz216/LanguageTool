﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    class CategoriesListViewModel : AddingListViewModel<ICategory>
    {
        public CategoriesListViewModel(BaseViewModel parent, List<ICategory> list)
            : base(parent, list)
        { }

        protected override void AddAction(object obj)
        {
            MainList = new ObservableCollection<ICategory>(DLLLoader.Instance.AddCategory(AddText));
            base.AddAction(obj);
        }

        protected override void RemoveAction(object obj)
        {
            if (SelectedItem != null)
            {
                MainList = new ObservableCollection<ICategory>(DLLLoader.Instance.RemoveCategory(SelectedItem));
                base.RemoveAction(obj);
            }
        }

        protected override void LostFocusAction(object obj)
        {
            MainList = new ObservableCollection<ICategory>(DLLLoader.Instance.UpdateCategory(SelectedItem));
        }

        protected override void TextChangedAction(object obj)
        {
            if (SearchText != null)
            {
                List = new ObservableCollection<ICategory>(from l in MainList where l.Name.StartsWith(SearchText) select l);
            }
        }
    }
}
