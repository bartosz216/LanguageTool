﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.PW_Projekt.UI.ViewModel.Command;
using Tatarski.PW_Projekt.UI.ViewModel.Common;

namespace Tatarski.PW_Projekt.UI.ViewModel
{
    public abstract class AddingListViewModel<T> : ListViewModel<T>
    {
        private string _addText;
        private CommonCommand _addCommand;


        public AddingListViewModel(BaseViewModel parent, List<T> list)
            : base(parent, list)
        {
        }

        public String AddText
        {
            get { return _addText; }
            set
            {
                _addText = value;
                OnPropertyChanged("AddText");
            }
        }

        public CommonCommand AddCommand
        {
            get
            {
                if (_addCommand == null)
                {
                    _addCommand = new CommonCommand(AddAction, 
                        param => !String.IsNullOrWhiteSpace(AddText));
                }

                return _addCommand;
            }
        }


        protected virtual void AddAction(object obj)
        {
            AddText = "";
            if (TextChangedCommand.CanExecute(null))
            {
                TextChangedCommand.Execute(null);
            }

            //OnPropertyChanged("MainList");
        }

    }
}
