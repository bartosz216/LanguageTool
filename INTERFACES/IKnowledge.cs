﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tatarski.Apka.INTERFACES
{
    public interface IKnowledge
    {
        int UserID { get; set; }
        int TranslateID { get; set; }
        int Solved { get; set; }
        int Unsolved { get; set; }
    }
}
