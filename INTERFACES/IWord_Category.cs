﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tatarski.Apka.INTERFACES
{
    public interface IWord_Category
    {
        int WordID { get; set; }
        int CategoryID { get; set; }
    }
}
