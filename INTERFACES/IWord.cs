﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tatarski.Apka.INTERFACES
{
    public interface IWord
    {
        int ID { get; set; }
        int DictionaryID { get; set; }
        string Name { get; set; }
    }
}
