﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Tatarski.Apka.INTERFACES
{
    public interface IUser
    {
        int ID { get; set; }
        string Name { get; set; }
        string Password { get; set; }
    }
}
