﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tatarski.Apka.INTERFACES
{
    public interface ITranslate
    {
        int ID { get; set; }
        int Word1ID { get; set; }
        int Word2ID { get; set; }
    }
}
