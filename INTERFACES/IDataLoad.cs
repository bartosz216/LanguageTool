﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Tatarski.Apka.INTERFACES
{
    public interface IDataLoad
    {

        List<ICategory> Categories { get; }
        List<IDictionary> Dictionaries { get; }
        List<IKnowledge> Statistics { get; }
        List<ITranslate> Translates { get; }
        IUser User { get; }
        List<IWord> Words { get; }
        List<IWord_Category> WordsCategories { get; }
        bool IsLogin { get; }

        bool Login(string name, string password);
        bool Logout();

        bool AddUser(string userName, string userPassword);
        List<ICategory> AddCategory(string categoryName);
        List<IDictionary> AddDictionary(string dictionaryName);
        List<IKnowledge> AddKnowledge(IWord word1, IWord word2, bool isCorrect);
        List<ITranslate> AddTranslate(IWord word1, IWord word2);
        List<IWord> AddWord(string wordName, IDictionary dictionary);
        List<IWord_Category> AddWordCategory(IWord word, ICategory category);

        List<ICategory> RemoveCategory(ICategory category);
        List<IDictionary> RemoveDictionary(IDictionary dictionaty);
        bool RemoveStatistics();
        List<ITranslate> RemoveTranslate(ITranslate translate);
        bool RemoveUser();
        List<IWord> RemoveWord(IWord word);
        List<IWord_Category> RemoveWordCategory(IWord word, ICategory category);

        List<ICategory> UpdateCategory(ICategory category);
        List<IDictionary> UpdateDictionaty(IDictionary dictionary);
        List<IKnowledge> UpdateStatistics(IKnowledge knowledge);
        List<ITranslate> UpdateTranslate(ITranslate translate);
        IUser UpdateUser(string newPassword);
        List<IWord> UpdateWord(IWord word);

        void GetWord(IDictionary dic1, IDictionary dic2, ICategory cat, out IWord word1, out IWord word2);

    }
}
