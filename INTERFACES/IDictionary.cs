﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tatarski.Apka.INTERFACES
{
    public interface IDictionary
    {
        int ID { get; set; }
        string Name { get; set; }
    }
}
