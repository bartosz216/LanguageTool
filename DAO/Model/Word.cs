﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;

namespace Tatarski.Apka.DAO.Model
{
    public class Word : IWord
    {
        public int ID { get; set; }
        public int DictionaryID { get; set; }
        public string Name { get; set; }
    }
}
