﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;

namespace Tatarski.Apka.DAO.Model
{
    public class Category : ICategory
    {
        public int ID { get; set; }
        public string Name { get; set; }

    }
}
