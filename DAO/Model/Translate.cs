﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;

namespace Tatarski.Apka.DAO.Model
{
    public class Translate : ITranslate
    {
        public int ID { get; set; }
        public int Word1ID { get; set; }
        public int Word2ID { get; set; }
    }
}
