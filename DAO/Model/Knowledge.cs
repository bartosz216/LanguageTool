﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;

namespace Tatarski.Apka.DAO.Model
{
    public class Knowledge : IKnowledge
    {
        public int UserID { get; set; }
        public int TranslateID { get; set; }
        public int Solved { get; set; }
        public int Unsolved { get; set; }
    }
}
