﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tatarski.Apka.INTERFACES;

namespace Tatarski.Apka.DAO.Model
{
    public class Word_Category : IWord_Category
    {
        public int WordID { get; set; }
        public int CategoryID { get; set; }
    }
}
