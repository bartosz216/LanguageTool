﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Tatarski.Apka.INTERFACES;

namespace Tatarski.Apka.DAO.Model
{
    public class DataLoader : IDataLoad
    {

        #region FileNames

        private const string usersFile = @"Users.xml";
        private const string categoriesFile = @"Categories.xml";
        private const string dictionariesFile = @"Dictionaries.xml";
        private const string translatesFile = @"Translates.xml";
        private const string wordCategoryFile = @"Word_Category.xml";
        private const string wordsFile = @"Words.xml";
        private const string knowledgeFile = @"Knowledge.xml";

        #endregion

        #region Fields

        private readonly List<Category> _categories;
        private readonly List<Dictionary> _dictionaries;
        private readonly List<Knowledge> _statistics;
        private readonly List<Translate> _translates;
        private User _user;
        private readonly List<Word> _words;
        private readonly List<Word_Category> _wordsCategories;
        private readonly Random r = new Random();

        #endregion

        #region Constructors

        public DataLoader()
        {
            _categories = ReadAllCategories();
            _dictionaries = ReadAllDictionary();
            _translates = ReadAllTranslates();
            _words = ReadAllWords();
            _wordsCategories = ReadAllWordsCategories();

            CheckeInitList(ref _categories);
            CheckeInitList(ref _dictionaries);
            CheckeInitList(ref _statistics);
            CheckeInitList(ref _translates);
            CheckeInitList(ref _words);
            CheckeInitList(ref _wordsCategories);
        }

        private void CheckeInitList<T>(ref List<T> list)
        {
            if (list == null)
                list = new List<T>();
        }

        #endregion

        #region Properties

        public List<ICategory> Categories
        {
            get
            {
                if (_categories == null)
                    return null;

                return new List<ICategory>(_categories.Cast<ICategory>());
            }
        }

        public List<IDictionary> Dictionaries
        {
            get
            {
                if (_dictionaries == null)
                    return null;

                return new List<IDictionary>(_dictionaries.Cast<IDictionary>());
            }
        }

        public List<IKnowledge> Statistics
        {
            get
            {
                if (_statistics == null)
                    return null;

                return new List<IKnowledge>(_statistics.Cast<IKnowledge>());
            }
        }

        public List<ITranslate> Translates
        {
            get
            {
                if (_statistics == null)
                    return null;

                return new List<ITranslate>(_translates.Cast<ITranslate>());
            }
        }

        public IUser User
        {
            get { return _user; }
        }

        public List<IWord> Words
        {
            get
            {
                if (_words == null)
                    return null;

                return new List<IWord>(_words.Cast<IWord>());
            }
        }

        public List<IWord_Category> WordsCategories
        {
            get
            {
                if (_wordsCategories == null)
                    return null;

                return new List<IWord_Category>(_wordsCategories.Cast<IWord_Category>());
            }
        }

        public bool IsLogin
        {
            get { return _user != null; }
        }

        #endregion

        #region Methods

        public bool SaveToFile<T>(string fileName, List<T> list)
        {
            if (list != null && fileName != null)
            {
                try
                {
                    TextWriter textWriter = new StreamWriter(fileName);
                    XmlSerializer serializer = new XmlSerializer(typeof(List<T>));

                    serializer.Serialize(textWriter, list);
                    textWriter.Close();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                    return false;
                }
                return true;
            }
            return false;
        }


        private List<T> ReadAll<T>(string fileName)
        {
            List<T> List = null;
            try
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(List<T>));
                TextReader textReader = new StreamReader(fileName);
                List = (List<T>)deserializer.Deserialize(textReader);
                textReader.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }

            return List;
        }

        private List<Category> ReadAllCategories()
        {
            return ReadAll<Category>(categoriesFile);
        }

        private List<Dictionary> ReadAllDictionary()
        {
            return ReadAll<Dictionary>(dictionariesFile);
        }

        private List<Translate> ReadAllTranslates()
        {
            return ReadAll<Translate>(translatesFile);
        }

        private List<User> ReadAllUsers()
        {
            return ReadAll<User>(usersFile);
        }

        private List<Word_Category> ReadAllWordsCategories()
        {
            return ReadAll<Word_Category>(wordCategoryFile);
        }

        private List<Word> ReadAllWords()
        {
            return ReadAll<Word>(wordsFile);
        }

        private List<Knowledge> ReadAllKnowledge()
        {
            return ReadAll<Knowledge>(knowledgeFile);
        }

        #endregion

        #region IDataLoad Implements

        public List<ICategory> AddCategory(string categoryName)
        {
            if (_categories != null)
            {
                int nextID = 0;
                if (_categories.Count > 0)
                {
                    nextID = _categories.Max(c => c.ID) + 1;
                }

                var containsCategories = _categories.Where(c => (
                    c.Name == categoryName
                    ));

                if (containsCategories.Count() == 0)
                {
                    _categories.Add(new Category()
                    {
                        ID = nextID,
                        Name = categoryName
                    });

                    SaveToFile(categoriesFile, _categories);

                }
            }

            return Categories;

        }

        public List<IDictionary> AddDictionary(string dictionaryName)
        {
            if (_dictionaries != null)
            {
                int nextID = 0;
                if (_dictionaries.Count > 0)
                {
                    nextID = _dictionaries.Max(c => c.ID) + 1;
                }


                var containsDictionaries = _dictionaries.Where(c => (
                    c.Name == dictionaryName
                    ));

                if (containsDictionaries.Count() == 0)
                {
                    _dictionaries.Add(new Dictionary()
                    {
                        ID = nextID,
                        Name = dictionaryName
                    });

                    SaveToFile(dictionariesFile, _dictionaries);
                }
            }

            return Dictionaries;
        }

        public List<IKnowledge> AddKnowledge(IWord word1, IWord word2, bool isCorrect)
        {
            if (_statistics != null && _user != null)
            {
                var containsTranslates = _translates.Where(c => (
                    (c.Word1ID == word1.ID
                    && c.Word2ID == word2.ID)
                    || (c.Word1ID == word2.ID
                    && c.Word2ID == word1.ID)
                ));
                if (containsTranslates.Count() > 0)
                {
                    Translate translate = containsTranslates.First();
                    var containsKnowledge = _statistics.Where(k =>
                        k.TranslateID == translate.ID &&
                        k.UserID == _user.ID);
                    Knowledge knowledge = null;
                    if (containsKnowledge.Count() > 0)
                    {
                        knowledge = containsKnowledge.First();
                    }
                    else
                    {
                        knowledge = new Knowledge()
                        {
                            TranslateID = translate.ID,
                            UserID = _user.ID,
                            Solved = 0,
                            Unsolved = 0
                        };

                    }

                    if (isCorrect)
                        knowledge.Solved++;
                    else
                        knowledge.Unsolved++;

                    UpdateStatistics(knowledge);
                }
            }

            return Statistics;
        }

        public List<ITranslate> AddTranslate(IWord word1, IWord word2)
        {
            if (_translates != null)
            {
                int nextID = 0;
                if (_translates.Count > 0)
                {
                    nextID = _translates.Max(c => c.ID) + 1;
                }
               

                var containsTranslates = _translates.Where(c => (
                    (c.Word1ID == word1.ID
                    && c.Word2ID == word2.ID)
                    || (c.Word1ID == word2.ID 
                    && c.Word2ID == word1.ID)
                    ));

                if (containsTranslates.Count() == 0)
                {
                    _translates.Add(new Translate()
                    {
                        ID = nextID,
                        Word1ID = word1.ID,
                        Word2ID = word2.ID
                    });

                    SaveToFile(translatesFile, _translates);
                }
            }
            return Translates;
        }

        public bool AddUser(string userName, string userPassword)
        {
            List<User> users = ReadAllUsers();
            int nextID = 0;
            if (users == null)
            {
                users = new List<User>();
            }
            else
            {
                nextID = users.Max(u => u.ID) + 1;
            }

            var containsUser = users.Where(u => (
                (u.Name == userName)
                ));

            if (containsUser.Count() == 0)
            {
                users.Add(new User()
                {
                    ID = nextID,
                    Name = userName,
                    Password = userPassword,
                });

                return SaveToFile(usersFile, users);
            }
            else
                throw new Exception("Użytkownik o podanej nazwie już istnieje");

        }

        public List<IWord> AddWord(string wordName, IDictionary dictionary)
        {
            if (_words != null)
            {
                int nextID = 0;
                if (_words.Count > 0)
                {
                    nextID = _words.Max(c => c.ID) + 1;
                }

                var containsWords = _words.Where(c => (
                    c.Name == wordName
                    ));

                if (containsWords.Count() == 0)
                {
                    _words.Add(new Word()
                    {
                        ID = nextID,
                        Name = wordName,
                        DictionaryID = dictionary.ID
                    });

                    SaveToFile(wordsFile, _words);

                }
            }
            return Words;
        }

        public List<IWord_Category> AddWordCategory(IWord word, ICategory category)
        {
            if (_wordsCategories != null)
            {
                var containsWordsCategories = _wordsCategories.Where(c => (
                    c.WordID == word.ID
                    && c.CategoryID == category.ID
                    ));

                if (containsWordsCategories.Count() == 0)
                {
                    _wordsCategories.Add(new Word_Category()
                    {
                        CategoryID = category.ID,
                        WordID = word.ID
                    });

                    SaveToFile(wordCategoryFile, _wordsCategories);
                }
            }
            return WordsCategories;
        }


        public bool Login(string name, string password)
        {
            List<User> users = ReadAllUsers();

            if (users != null)
            {
                foreach (User u in users)
                {
                    if ((u.Name == name) && ((u.Password == password) || (string.IsNullOrEmpty(u.Password) == string.IsNullOrEmpty(password)) ))
                    {
                        _user = u;
                        ReadStatistics();
                        return true;
                    }
                }
            }
            return false;
        }

        public bool Logout()
        {
            if (_user != null)
            {
                List<User> users = ReadAllUsers();
                if (users != null)
                {
                    var toDelete = users.Where(u => u.ID == _user.ID).ToList();
                    foreach (User user in toDelete)
                        users.Remove(user);

                    users.Add(_user);
                    _user = null;
                    return SaveToFile(usersFile, users);
                }
            }
            return false;
        }

        private void ReadStatistics()
        {
            if (_user != null)
            {
                _statistics.Clear();
                var statistics = ReadAllKnowledge();
                if (statistics != null)
                {
                    _statistics.AddRange(from k in statistics where k.UserID == _user.ID select k);
                }
            }

        }

        public List<ICategory> RemoveCategory(ICategory category)
        {
            if (_categories != null && category != null)
            {
                if (_categories.Contains(category))
                {
                    var catToDelete = _categories.Where(p => p.ID == category.ID).ToList();
                    foreach (Category cat in catToDelete)
                        _categories.Remove(cat);

                    SaveToFile(categoriesFile, _categories);

                    if (_wordsCategories != null)
                    {
                        var wcToDelete = _wordsCategories.Where(wc => wc.CategoryID == category.ID).ToList();
                        foreach (Word_Category wc in wcToDelete)
                            _wordsCategories.Remove(wc);

                        SaveToFile(wordCategoryFile, _wordsCategories);
                    }
                }
            }

            return Categories;
        }

        public List<IDictionary> RemoveDictionary(IDictionary dictionary)
        {
            if (_dictionaries != null && dictionary != null)
            {
                if (_dictionaries.Contains(dictionary))
                {
                    var dicToDelete = _dictionaries.Where(d => d.ID == dictionary.ID).ToList();
                    foreach (Dictionary dic in dicToDelete)
                        _dictionaries.Remove(dic);

                    SaveToFile(dictionariesFile, _dictionaries);

                    if (_words != null)
                    {
                        List<Knowledge> statistics = ReadAllKnowledge();
                        bool trabslatesChanged = false;
                        bool wordsCategoriesChanged = false;

                        var wordsToDelete = _words.Where(w => w.DictionaryID == dictionary.ID).ToList();
                        foreach (Word word in wordsToDelete)
                        {
                            if (_translates != null)
                            {
                                var transToDelete = _translates.Where(t => t.Word1ID == word.ID || t.Word2ID == word.ID).ToList();
                                foreach (Translate translate in transToDelete)
                                {
                                    _translates.Remove(translate);

                                    if (statistics != null)
                                    {
                                        var statToDelete = statistics.Where(k => k.TranslateID == translate.ID).ToList();
                                        foreach (Knowledge k in statToDelete)
                                            statistics.Remove(k);
                                    }

                                    trabslatesChanged = true;
                                }
                            }

                            if (_wordsCategories != null)
                            {
                                var wcToDelete = _wordsCategories.Where(wc => wc.WordID == word.ID).ToList();
                                foreach (Word_Category wc in wcToDelete)
                                    _wordsCategories.Remove(wc);
                                wordsCategoriesChanged = true;
                            }

                            _words.Remove(word);
                        }

                        if (trabslatesChanged)
                        {
                            SaveToFile(translatesFile, _translates);
                            SaveToFile(knowledgeFile, statistics);

                            if (_user != null && statistics != null)
                            {
                                _statistics.Clear();
                                _statistics.AddRange(from k in statistics where k.UserID == k.UserID select k);
                            }
                        }

                        if (wordsCategoriesChanged)
                        {
                            SaveToFile(wordCategoryFile, _wordsCategories);
                        }

                        SaveToFile(wordsFile, _words);
                    }
                }
            }

            return Dictionaries;
        }

        public bool RemoveStatistics()
        {
            if (_user == null)
            {
                List<Knowledge> statistics = ReadAllKnowledge();
                if (statistics != null)
                {
                    var toDelete = statistics.Where(k => k.UserID == _user.ID).ToList();
                    foreach (Knowledge k in toDelete)
                        statistics.Remove(k);

                    return SaveToFile(knowledgeFile, statistics);
                }
            }

            return false;
        }

        public List<ITranslate> RemoveTranslate(ITranslate translate)
        {
            if (_translates != null && translate != null)
            {
                if (_translates.Contains(translate))
                {
                    var toDelete = _translates.Where(t => t.ID == translate.ID).ToList();
                    foreach (Translate t in toDelete)
                        _translates.Remove(t);

                    SaveToFile(translatesFile, _translates);
                }
            }

            return Translates;
        }

        public bool RemoveUser()
        {
            if (_user != null)
            {
                List<User> users = ReadAllUsers();
                if (users.Where( u => _user.ID == u.ID).Count() > 0)
                {
                    RemoveStatistics();
                    users.RemoveAll(u => u.ID == _user.ID);
                    _user = null;
                    return SaveToFile(usersFile, users);
                }
            }

            return false;
        }

        public List<IWord> RemoveWord(IWord word)
        {
            if (word != null && _words != null)
            {
                if (_words.Contains(word))
                {
                    if (_translates != null)
                    {
                        List<Knowledge> statistics = ReadAllKnowledge();

                        var translateToDelete = _translates.Where(t => t.Word1ID == word.ID || t.Word2ID == word.ID).ToList();
                        foreach (Translate translate in translateToDelete)
                        {
                            _translates.Remove(translate);
                            if (statistics != null)
                            {
                                var statToDelete = statistics.Where(k => k.TranslateID == translate.ID).ToList();
                                foreach (Knowledge k in statToDelete)
                                    statistics.Remove(k);
                            }
                        }

                        SaveToFile(translatesFile, _translates);
                        SaveToFile(knowledgeFile, statistics);

                        if (_user != null && statistics != null)
                        {
                            _statistics.Clear();
                            _statistics.AddRange(from k in statistics where k.UserID == k.UserID select k);
                        }
                    }

                    if (_wordsCategories != null)
                    {
                        var wcToDelete = _wordsCategories.Where(wc => wc.WordID == word.ID).ToList();
                        foreach (Word_Category wc in wcToDelete)
                            _wordsCategories.Remove(wc);
                        SaveToFile(wordCategoryFile, _wordsCategories);
                    }

                    var wordsToDelete = _words.Where(w => w.ID == word.ID).ToList();
                    foreach (Word w in wordsToDelete)
                        _words.Remove(w);
                    SaveToFile(wordsFile, _words);
                }
            }
            return Words;
        }

        public List<IWord_Category> RemoveWordCategory(IWord word, ICategory category)
        {
            if (word != null && category != null  && _wordsCategories != null)
            {

                if (_wordsCategories.Where(wc => 
                    wc.CategoryID == category.ID && wc.WordID == word.ID
                    ).Count() > 0)
                {
                    var wcToDelete = _wordsCategories.Where(wc => 
                        wc.WordID == word.ID && wc.CategoryID == category.ID
                        ).ToList();

                    foreach (Word_Category wc in wcToDelete)
                        _wordsCategories.Remove(wc);
                    SaveToFile(wordCategoryFile, _wordsCategories);
                }

            }
            return WordsCategories;
        }


        public List<ICategory> UpdateCategory(ICategory category)
        {
            if (_categories != null && category != null)
            {
                var toDelete = _categories.Where(c => c.ID == category.ID).ToList();
                foreach (Category c in toDelete)
                    _categories.Remove(c);

                _categories.Add(new Category()
                {
                    ID = category.ID,
                    Name = category.Name
                });

                SaveToFile(categoriesFile, _categories);
            }



            return Categories;
        }

        public List<IDictionary> UpdateDictionaty(IDictionary dictionary)
        {
            if (_dictionaries != null && dictionary != null)
            {
                var toDelete = _dictionaries.Where(d => d.ID == dictionary.ID).ToList();
                foreach (Dictionary d in toDelete)
                    _dictionaries.Remove(d);

                _dictionaries.Add(new Dictionary()
                {
                    ID = dictionary.ID,
                    Name = dictionary.Name
                });

                SaveToFile(dictionariesFile, _dictionaries);
            }

            return Dictionaries;
        }

        public List<IKnowledge> UpdateStatistics(IKnowledge knowledge)
        {
            if (knowledge != null)
            {
                List<Knowledge> statistics = ReadAllKnowledge();
                if (statistics != null)
                {

                    var toDelete = statistics.Where(k =>
                        k.UserID == knowledge.UserID &&
                        k.TranslateID == knowledge.TranslateID
                        ).ToList();

                    foreach (Knowledge k in toDelete)
                    {
                        statistics.Remove(k);
                    }
                }
                else
                {
                    statistics = new List<Knowledge>();
                }
              
                statistics.Add(new Knowledge()
                {
                    Solved = knowledge.Solved,
                    TranslateID = knowledge.TranslateID,
                    Unsolved = knowledge.Unsolved,
                    UserID = knowledge.UserID
                });

                SaveToFile(knowledgeFile, statistics);

                if(_user != null)
                {
                    _statistics.Clear();
                    _statistics.AddRange(from k in statistics where k.UserID == _user.ID select k);
                }
            }

            return Statistics;
        }

        public List<ITranslate> UpdateTranslate(ITranslate translate)
        {
            if (_translates != null && translate != null)
            {
                var toDelete = _translates.Where(t => t.ID == translate.ID).ToList();
                foreach (Translate t in toDelete)
                    _translates.Remove(t);
                _translates.Add(new Translate()
                {
                    ID = translate.ID,
                    Word1ID = translate.Word1ID,
                    Word2ID = translate.Word2ID
                });

                SaveToFile(translatesFile, _translates);
            }

            return Translates;
        }

        public IUser UpdateUser(string newPassword)
        {
            if (_user != null)
            {
                _user.Password = newPassword;
                List<User> users = ReadAllUsers();
                if (users != null)
                {
                    var toDelete = users.Where(u => u.ID == _user.ID).ToList();
                    foreach (User u in toDelete)
                        users.Remove(u);
                    users.Add(_user);

                    SaveToFile(usersFile, users);
                }
            }
            return User;
        }

        public List<IWord> UpdateWord(IWord word)
        {
            if (word != null && _words != null)
            {
                var toDelete = _words.Where(w => w.ID == word.ID).ToList();
                foreach (Word w in toDelete)
                    _words.Remove(w);
                _words.Add(new Word()
                {
                    ID = word.ID,
                    DictionaryID = word.DictionaryID,
                    Name = word.Name
                });

                SaveToFile(wordsFile, _words);
            }

            return Words;
        }

        public void GetWord(IDictionary dic1, IDictionary dic2, ICategory cat, out IWord word1, out IWord word2)
        {
            var wordsDic1 = (from w in _words
                             from wc in _wordsCategories
                             where w.DictionaryID == dic1.ID && wc.WordID == w.ID &&
                             wc.CategoryID == cat.ID
                             select w);
            var wordsDic2 = (from w in _words
                             from wc in _wordsCategories
                             where w.DictionaryID == dic2.ID && wc.WordID == w.ID &&
                             wc.CategoryID == cat.ID
                             select w);

            var translates = (from w1 in wordsDic1
                              from w2 in wordsDic2
                              from t in _translates
                              where (w1.ID == t.Word1ID && w2.ID == t.Word2ID) ||
                              (w1.ID == t.Word2ID && w2.ID == t.Word1ID)
                              select t);
            if (wordsDic1.Count() > 0 && wordsDic2.Count() > 0 && translates.Count() > 0)
            {
                var translate = translates.ElementAt(r.Next(translates.Count()));

                word1 = (from w in wordsDic1
                         where w.ID == translate.Word1ID || w.ID == translate.Word2ID
                         select w).First();

                word2 = (from w in wordsDic2
                         where w.ID == translate.Word1ID || w.ID == translate.Word2ID
                         select w).First();
            }
            else
            {
                word1 = null;
                word2 = null;
            }
        }

        #endregion
    }
}
